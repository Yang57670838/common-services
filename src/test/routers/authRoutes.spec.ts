import should = require('should');
import mongoose = require('mongoose');
import request = require('supertest');

import {User} from '../../models/User';
import app from '../../App';
//test database
const connectionString = 'mongodb://127.0.0.1/commonServicesTest';

describe('Signup', function() {
    //make a connection to the databse before test signup
    //also clean up whole database before all tests in this block
    before('clean up and setup singup', function(done) {
        mongoose.connect(connectionString, function (err) {
            if(err) return done(err);
            //instead of delete every test info, just drop whole database here first
            mongoose.connection.db.dropDatabase(done);
        })
    })
    it('/api/auth/signup', function(done) {
        request(app)
           .post('/api/auth/signup')
           .send({
            name: 'testname',
            password: 'pass',
            email: 'testname@mail.com',
            phone: '00000',
            role: 'tester bot'
        })
           .expect(200)
           .end(function(err, response) {
               if (err) {
                   return done(err)
               }
               should(response.body.name).be.a.String();
               done();
           })
    })
})

describe('Signin', function() {

    before('clean up and setup signin user', function(done) {
        mongoose.connect(connectionString, function() {
            mongoose.connection.db.dropDatabase();
            
            User.create({
                name: 'testname',
                password: 'pass',
                email: 'testname@mail.com',
                phone: '00000',
                role: 'tester bot'
            }, done)
        })
    })

    it('/api/auth/signin should be successful', function (done) {
        request(app)
            .post('/api/auth/signin')
            .send({
                password: 'pass',
                email: 'testname@mail.com'
            })
            .expect(200)
            .end(function (err, response) {
                if (err) return done(err)
                should(response.body.token).be.a.String();
                done()
            })

    })

    it('/api/auth/signin should fail with wrong password', function (done) {
        request(app)
            .post('/api/auth/signin')
            .send({
                password: 'wrongpassword',
                email: 'testname@mail.com'
            })
            .expect(403)
            .end(function (err, response) {
                if (err) return done(err)
                should(response.body.token).not.be.a.String();
                should(response.body.message).be.equal('email or password incorrect');
                done()
            })

    })

})

describe('getCurrentRole', function() {

    let authToken;
    before('clean up and setup signin user', function(done) {
        mongoose.connect(connectionString, function() {
            mongoose.connection.db.dropDatabase();
            
            User.create({
                name: 'testname2',
                password: 'pass',
                email: 'testname2@mail.com',
                phone: '00000',
                role: 'tester bot'
            }, function (err, createdUser) {
                if (err) return done(err)

                request(app)
                    .post('/api/auth/signin')
                    .send({
                        password: 'pass',
                        email: 'testname2@mail.com'
                    })
                    .expect(200)
                    .end(function (err, response) {
                        if (err) return done(err)
                        should(response.body.token).be.a.String();

                        authToken = response.body.token;
                        done()
                    })
            })
        })
    })

    it('/api/auth/getCurrentRole should be successful', function (done) {
        request(app)
            .get('/api/auth/getCurrentRole')
            .set('Authorization', authToken)
            .expect(200)
            .end(function (err, response) {
                if (err) return done(err)
                should(response.body).containEql({
                    name: 'testname2',
                    email: 'testname2@mail.com'
                });
                done()
            })

    })

})