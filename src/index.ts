process.env.UV_THREADPOOL_SIZE = '6';

import app from './App'
import { connect } from './database/dbConnection'

//connect to database when start the server..
connect();
app.listen(3000, 'localhost', function (err) {
    if (err) {
        console.log('error starting server', + err)
        return;
    }

    console.log('server started successfully on port 3000')
});