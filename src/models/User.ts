import {Schema, model, Document} from 'mongoose';

export interface IUser {
    name: String,
    email: String,
    password: String,
    phone: String,
    role: String
}

export interface IUserDocument extends Document, IUser {}

var userSchema = new Schema({
   name: String, 
   email: {type: String, unique: true},
   password: String,
   phone: {type: String, unique: true},
   role: String
})

export var User = model<IUserDocument>('User', userSchema)