import {Router, Request, Response} from 'express';
import {User} from '../models/User';
import {Session} from '../models/Session';
import uuidv1 = require('uuid/v1');

import {isAuthenticated} from '../auth/authMiddleware';

var router = Router();

router.post('/signup', async (req: Request, res:Response) => {
    //To do: confirm the sign up requirements.....
    try {
        var createdUser = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            phone: req.body.phone,
            role: req.body.role
        })
    } catch (err) {
        return res.status(401).send(err);
    }

    res.send(createdUser);
})

router.post('/signin', async (req: Request, res:Response) => {

    //create the node-uuid as session id and save it, it will be used as a token
    try {
        var foundUser = await User.findOne({
            email: req.body.email
        }).exec();

        if (foundUser.password !== req.body.password) {
            return res.status(403).send({message: 'email or password incorrect'});
        }

        //create session now
        //To do: add session expire date later..
        var createdSession = await Session.create({
            sid: uuidv1(),
            user: foundUser._id
        });

        console.log('New Created Session', createdSession.sid);
        return res.send({token: createdSession.sid});

    } catch (err) {
        return res.status(401).send(err);
    }

})

router.get('/getCurrentRole', isAuthenticated, (req: Request, res:Response) => {
    //remove the password before send back
    if (req['user'].password) {
        req['user'].password = '';
    }
    res.send(req['user']);
})

export default router;