import express = require('express');
import bodyParser = require('body-parser');
import {Application} from 'express';

const app: Application = express();
import authRoutes from './routes/authRoutes';
import {authMiddleware} from './auth/authMiddleware';

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(authMiddleware);
app.use('/api/auth', authRoutes);
app.get('/test', function(req, res) {
    res.send('test here');
});

export default app;