import {Session} from '../models/Session';
import {Request, Response} from 'express';

export function authMiddleware (req: Request, res: Response, next: Function) {

    if (req.header('Authorization')) {
        Session.findOne({sid: req.header('Authorization')})
        .populate('user')
        .exec(function (err, foundSession) {
            if (foundSession) {
                req['user'] = foundSession.user;
                req['isAuthenticated'] = true;
                return next();
            } else {
               req['isAuthenticated'] = false;
               req['user'] = null;
               return next();
            }

        })
    } else {
        req['isAuthenticated'] = false;
        req['user'] = null;
        return next();
    }
    
}

export function isAuthenticated (req: Request, res: Response, next: Function) {
    if (req['isAuthenticated'] == true && req['user']._id) {
        return next();
    } else {
        console.log(401);
        return res.status(401).send({message: 'authentication required'});
    }
}