import mongoose = require('mongoose');

mongoose.connection.on('connected', function () {
    console.log('mongoose connected');
});

mongoose.connection.on('err', function (e) {
    console.log('mongoose connection rror', e);
});

mongoose.connection.on('disconnected', function () {
    console.log('mongoose disconnected');
});

export function connect() {
   return mongoose.connect("mongodb://127.0.0.1/commonServices");
}