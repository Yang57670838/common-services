-- church setup 
CREATE TYPE ROLES AS ENUM ('staff', 'member');

CREATE TABLE people (
    ppl_id SERIAL PRIMARY KEY,
    first_name VARCHAR(30), 
    last_name VARCHAR(30),
    gender CHAR(1),
    date_of_birth DATE,
    ppl_role ROLES,
    family_id INT REFERENCES family (family_id),
    email VARCHAR(50),
    mobile VARCHAR(20) UNIQUE
);

CREATE TABLE income (
    income_id SERIAL PRIMARY KEY,
    income_details VARCHAR(50),
    income_amount NUMERIC(8,2),
    income_date DATE,
    ppl_id INT REFERENCES people (ppl_id)
); 

CREATE TABLE event (
    event_id SERIAL PRIMARY KEY,
    event_details VARCHAR(50),
    event_date DATE
);

CREATE TABLE family (
    family_id SERIAL PRIMARY KEY,
    is_visited BOOLEAN DEFAULT False,
    address VARCHAR(50),
    suburb VARCHAR(15),
    state VARCHAR(15),
    zip NUMERIC(4, 0)
);

-- junction
CREATE TABLE event_people (
    event_id INT REFERENCES event (event_id),
    ppl_id INT REFERENCES people (ppl_id),
    PRIMARY KEY (event_id,ppl_id)
);

